const { Template } = require('../helper/template')

class PuppeteerOptions {

    constructor(argv) {
        this.screenshot = !!argv.screenshot
        let defaultPath = "output.pdf"
        let defaultType = "pdf"
        if(!!argv.screenshot) defaultPath = "output.png"
        if(!!argv.screenshot) defaultType = "png"
        this.path = argv.output || defaultPath
        this.type = argv.type || defaultType
        this.format = argv.pageSize
        this.scale = argv.scale || 1
        this.pageRange = argv.pageRange;
        this.landscape = !!argv.landscape
        this.printBackground = !!argv.printBackground
        this.displayHeaderFooter = !!argv.printHeaderAndFooter
        if(!!argv.screenshot) this.captureBeyoundViewport = !argv.clipToViewport
        this.margin = {
            top: argv.marginTop || "18mm",
            left: argv.marginLeft || "18mm",
            right: argv.marginRight || "18mm",
            bottom: argv.marginBottom || "18mm"
        }
        this.timeout = parseInt(argv.timeout) * 1000
        this.viewport = {
            width: parseInt(argv.viewport.substring(0, argv.viewport.indexOf('x'))),
            height: parseInt(argv.viewport.substring(argv.viewport.indexOf('x') + 1))
        }
        this.ignoreCertErrors = argv.ignoreCertErrors
        this.sleepBeforePrint = parseInt(argv.sleepBeforePrint) * 1000
        const template = new Template(argv)
        if (template.hasHeader()) 
            this.headerTemplate = template.getHeader()
        if (template.hasFooter())
            this.footerTemplate = template.getFooter()
        if (argv.verbose)
            this.printOptions()
    }

    printOptions() {
        console.log(`Creating ${this.type} with following options:`, this)
    }

    addCookies(cookies) {
        this.cookies = cookies
    }

}

module.exports = { PuppeteerOptions }
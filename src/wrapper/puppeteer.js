const puppeteer = require('puppeteer')
const Path = require('path')
const { exit } = require('process')

class PuppeteerWrapper {

    constructor(url, options, cookies) {
        this.options = options
        this.url = url
        this.cookies = cookies
        this.browser = Object
    }

    async printPDF() {
        let error = 0
        try {
            this.browser = await this.getBrowserInstance()
            const page = await this.browser.newPage()
            
            await page.setViewport({
                width: parseInt(this.options.viewport.width),
                height: parseInt(this.options.viewport.height),
            })
            if(this.cookies.hasCookies() && !this.cookies.requireReload())
                await page.setCookies(this.cookies.getCookies())
            await page.goto(this.url, { waitUntil: 'networkidle2' });
            if(this.cookies.hasCookies() && this.cookies.requireReload()) {
                if (this.cookies.options.verbose)
                        console.warn("[warning] One or more cookies are missing url or path. Need to reload page. This will increase runtime!")
                await page.setCookie(...this.cookies.getCookies())
                await page.reload({
                    waitUntil: 'networkidle2',
                    timeout: this.options.timeout
                })
            }
            if(this.cookies.options.verbose)
                console.log("Cookies in use:", await page.cookies())
            
            if(this.options.screenshot) {
                await page.screenshot(this.options)
            } else {
                await page.pdf(this.options);
            }
        } catch(e) {
            console.error(e)
        } finally {
            await this.browser.close().finally(() => { true });
        }
    }

    async getBrowserInstance() {
        const path = Path.join(process.cwd(), '.qfqpdf-chromium')
        const fetcher = puppeteer.createBrowserFetcher({path: path})
        // Latest supported Revision Number is usually in one of the release notes here: https://github.com/puppeteer/puppeteer/releases
        const { local, revision, executablePath } = fetcher.revisionInfo('1281684') // 1281684 1181205
       
        
        if (!local) {
            if (this.cookies.options.verbose)
                console.log("Downloading chrome browser", { local, revision, executablePath})
            await fetcher.download(revision)
        } 
      
        return await puppeteer.launch({
            executablePath: executablePath,
            waitForInitialPage: false,
            defaultViewport: {
                width: this.options.viewport.width,
                height: this.options.viewport.height
            },
            ignoreHTTPSErrors: this.options.ignoreCertErrors,
            acceptInsecureCerts: this.options.ignoreCertErrors,
            args: ['--no-sandbox', '--disable-setuid-sandbox', `--window-size ${this.options.viewport.width}, ${this.options.viewport.height}`],
        })
    }
}

module.exports = { PuppeteerWrapper };
#!/usr/bin/env node

const { PuppeteerWrapper } = require('./wrapper/puppeteer')
const { PuppeteerOptions } = require('./wrapper/puppeteerOptions')
const argv = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const { Cookies } = require('./helper/cookies')
const { exit } = require('yargs')

const arguments = argv(hideBin(process.argv))
  .command('$0 <source> [output]', 'Generate PDF files from URL using puppeteer', (argv) => {
    return argv
      .positional('output', {
        describe: 'output file to generate',
        default: "output.pdf"
      })
      .positional('source', {
        describe: 'URL to be printed'
      })
  }, (argv) => {
    if (argv.verbose) console.info(`generating pdf from: "${argv.source}" to: ${argv.output}`)
    /* program to run */
  })
  .option('screenshot', {
    alias: 's',
    type: 'boolean',
    description: 'Take a screenshot instead (default output changes to: output.png)'
  })
  .option('cookies', {
      alias: 'c',
      type: 'array',
      description: 'multiple cookie strings inline, ex: "key:value,key:value" "key:value,key:value"'
  })
  .option('cookie-file', {
      type: 'string',
      description: 'path to a JSON file with multiple cookies'
  })
  .option('print-header-and-footer', {
      alias: 'F',
      type: 'boolean',
      description: 'Print with header and footer'
  })
  .option('print-background', {
      alias: 'b',
      type: 'boolean',
      description: 'Print background images'
  })
  .option('landscape', {
    alias: 'l',
    type: 'boolean',
    description: 'Print in landscape'
  })
  .option('paper-size', {
      alias: "p",
      description: 'Set papersize to: A0 - A6, Letter, Legal, etc.',
      default: "A4",
      type: "string"
  })
  .option('scale', {
    alias: "S",
    description: "Scale page content, from 0.1 to 2",
    type: "number",
    default: 1
  })
  .option('page-ranges', {
    type: "string",
    description: "Page ranges to print, ex. \"0-5\"",
    default: "all"
  })
  .option('margin-top', {
    description: 'Page margin in units (px, mm, in)',
    type: 'string',
    default: "18mm"
  })
  .option('margin-bottom', {
    description: 'Page margin in units (px, mm, in)',
    type: 'string',
    default: "18mm"
  })
  .option('margin-left', {
    description: 'Page margin in units (px, mm, in)',
    type: 'string'
  })
  .option('margin-right', {
    description: 'Page margin in units (px, mm, in)',
    type: 'string',
    default: "18mm"
  })
  .option('template-header', {
    description: 'Provide an inline header template',
    type: 'string',
    default: "18mm"
  })
  .option('template-header-file', {
    description: 'Path to file containing header template',
    type: 'string'
  })
  .option('template-footer', {
    description: 'Provide an inline footer template',
    type: 'string'
  })
  .option('template-footer-file', {
    description: 'Path to file containing footer template',
    type: 'string'
  })
  .option('viewport', {
    alias: 'V',
    type: 'string',
    default: '1280x720',
    description: 'Viewport of the opened browser window'
  })
  .epilog('qfqPDF 2023, detailed information and sources: https://git.math.uzh.ch/typo3/qfqpdf')
  .argv

  const cookies = new Cookies(arguments)

  const options = new PuppeteerOptions(arguments);
  const puppeteerWrap = new PuppeteerWrapper(arguments.source, options, cookies)
  puppeteerWrap.printPDF()
const loadJsonFile = require('load-json-file');

class Cookies {
    
    constructor(argv) {
        this.options = argv
        this.cookies = []
    }

    makeCookies() {
        for(const rawCookie of this.options.cookies) {
            const cookieTable = rawCookie.split(",").map(pair => pair.split(":"));
            const bakedCookie = Object.fromEntries(cookieTable);
            this.cookies.push(bakedCookie);
        }
    }

    getCookiesFromJSON() {
        const jsonResponse = loadJsonFile(this.options.cookieFile)
        for(const cookie in jsonResponse.cookies) {
            this.cookies.push(cookie);
        }
    }

    setCookies() {
        if(this.options.cookies)
            this.makeCookies()
        if(this.options.cookieFile)
            this.getCookiesFromJSON()
    }

    getCookies() {
        if(this.options.cookies)
            this.makeCookies()
        if(this.options.cookieFile)
            this.getCookiesFromJSON()
        if(this.cookies.length > 0)
            return this.cookies
        return undefined
    }

    hasCookies() {
        if(this.options.cookies || this.options.cookieFile)
            return true
        return this.cookies.length > 0
    }

    requireReload() {
        if (this.cookies.length === 0) 
            this.setCookies()
        for(const cookie in this.cookies) {
            if (!cookie.hasOwnProperty("url") || !cookie.hasOwnProperty("path")) 
                return true
        }
        if(this.options.verbose)
            console.log("Cookies don't require page reload.")
        return false
    }
}

module.exports = { Cookies }
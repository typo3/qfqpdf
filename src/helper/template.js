const fileGetContent = require('file-get-contents');

class Template {

    constructor(argv) {
        this.header = {
            file: argv.templateHeaderFile,
            inline: argv.templateHeader
        }
        this.footer = {
            file: argv.templateFooterFile,
            inline: argv.templateFooter
        }
        this.verbose = !!argv.verbose
    }

    getHeader() {
        if(this.header.inline) 
            return this.header.inline
        if(this.header.file)
            return fileGetContent(this.header.file)
        return undefined
    }

    getFooter() {
        if(this.footer.inline)
            return this.footer.inline
        if(this.footer.file)
            return fileGetContent(this.footer.file)
        return undefined
    }

    hasHeader() {
        if (this.header.file === undefined && this.header.inline === undefined)
            return false
        if (this.verbose)
            console.log ("Using header from template.", this.header)
        return true
    }

    hasFooter() {
        if (this.footer.file === undefined && this.footer.inline === undefined)
            return false
        if (this.verbose)
            console.log ("Using footer from template.", this.footer)
        return true
    }
}

module.exports = { Template }
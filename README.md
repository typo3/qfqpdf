# qfqPDF

CLI Tool to create PDF and Screenshots using [puppeteer](https://developers.google.com/web/tools/puppeteer/)

# Installation

* Use most recent binary from: [releases page](https://www.math.uzh.ch/repo/?dir=qfqpdf) (see release notes below)
```
curl -L -o qfqpdf https://www.math.uzh.ch/repo/qfqpdf/current/qfqpdf-linux
chmod a+x qfqpdf
./qfqpdf --version
```
* Checkout this repository and run it with node or build it:
```
git clone 
cd qfqpdf
npm install
node . --help
```
* requires node v14

# Usage

Example:
```
./qfqpdf "https://www.math.uzh.ch" --page-size A5 --margin-top 22mm --template-header "<h1>Test</h1>" -Fvbc "name:firstCookie,value:tunjngvnjfr23r" "name:secondCookie,value:249ur4jn233d"
```

All commands can be listed with --help / -h

```
qfqpdf-linux <source> [output]

Generate PDF files from URL using puppeteer

Positionals:
  output                                                 [default: "output.pdf"]
  source

Options:
      --help                     Show help                             [boolean]
      --version                  Show version number                   [boolean]
  -s, --screenshot               Take a screenshot instead (default output
                                 changes to: output.png)               [boolean]
  -c, --cookies                  multiple cookie strings inline, ex:
                                 "key:value,key:value" "key:value,key:value"
                                                                         [array]
      --cookie-file              path to a JSON file with multiple cookies
                                                                        [string]
  -F, --print-header-and-footer  Print with header and footer          [boolean]
  -b, --print-background         Print background images               [boolean]
  -l, --landscape                Print in landscape                    [boolean]
  -p, --paper-size               Set papersize to: A0 - A6, Letter, Legal, etc.
                                                        [string] [default: "A4"]
  -S, --scale                    Scale page content, from 0.1 to 2
                                                           [number] [default: 1]
      --page-ranges              Page ranges to print, ex. "0-5"
                                                       [string] [default: "all"]
      --margin-top               Page margin in units (px, mm, in)
                                                      [string] [default: "18mm"]
      --margin-bottom            Page margin in units (px, mm, in)
                                                      [string] [default: "18mm"]
      --margin-left              Page margin in units (px, mm, in)      [string]
      --margin-right             Page margin in units (px, mm, in)
                                                      [string] [default: "18mm"]
      --template-header          Provide an inline header template
                                                      [string] [default: "18mm"]
      --template-header-file     Path to file containing header template[string]
      --template-footer          Provide an inline footer template      [string]
      --template-footer-file     Path to file containing footer template[string]
  -V, --viewport                 Viewport of the opened browser window
                                                  [string] [default: "1280x720"]

qfqPDF 2023, detailed information and sources:
https://git.math.uzh.ch/typo3/qfqpdf
```

# Cookie File

It is possible to provide a JSON file with all cookies, syntax is:
```
{
    cookies: [
        {
            "name": "first Cookie",
            "value": "49f33n3jr3jop23j",
            "url": "https://www.example.com",
            "path": "/"
        },
        {
            "name": "second Cookie",
            "value": "49f989uu313jr3jop23j",
            "url": "https://www.example.com",
            "path": "/"
        }
    ]
}
```

All Cookie values can be set. You can also combine inline cookies with static JSON file cookies.

# Templates

Templates for header can be supplied inline or with files. Please note, that while templates are supplied in html, they only should contain the body of a page.
You can access classes from the site you are printing for styling.

From the puppeteer documentation:

headerTemplate <string> HTML template for the print header. Should be valid HTML markup with following classes used to inject printing values into them:
 * `date` formatted print date
 * `title` document title
 * `url` document location
 * `pageNumber` current page number
 * `totalPages` total pages in the document

 Example: 
 ```
 --header-template '<div style="font-size:5mm;" class="pageNumber"></div>'
 ```


# Releasnotes

* v1.3.0 - finally added that closes chromium on error
* v1.2.0 - updated chromium, screenshot function added
* v1.1.0 - added sleep-before-print
* v1.0.5 - added timeout and waitUntil load
* v1.0.4 - exit on error
* v1.0.3 - added viewport & --ignore-cert-errors
* v1.0.2 - added page ranges, better --help
* v1.0.1 - fixed browser download if current chrome version isn't installed.
* v1.0.0 - first release
